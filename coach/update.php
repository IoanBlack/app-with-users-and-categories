<?php

require_once '../config/db.php';
require_once '../classes/Coach.php';


if (!empty($_POST['name']) & !empty($_POST['surname']) & !empty($_POST['email']) & !empty($_POST['chair']) & !empty($_POST['id'])) {
    Coach::deleteSubject($_POST['id'],$connection);
    Coach::upgrade($_POST['id'],$_POST['name'], $_POST['surname'], $_POST['email'],$_POST['chair'],$_POST['subjects']);

}
header('Location:../index.php');
