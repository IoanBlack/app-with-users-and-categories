<?php

require_once '../classes/Coach.php';

if(!empty($_POST['name']) & !empty($_POST['surname']) & !empty($_POST['email'])) {


    $coach = new Coach($_POST['name'],$_POST['surname'],$_POST['email'],$_POST['chair']);
    $coach->store();
    $coach->addSubject($_POST['subjects']);

    header('Location:../');
}
