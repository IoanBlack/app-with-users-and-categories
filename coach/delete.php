<?php

require_once '../config/db.php';
require_once '../classes/Coach.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    Coach::deleteSubject($id,$connection);
    Coach::delete($id, $connection);

    header('Location:../');
}

