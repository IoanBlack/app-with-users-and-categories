<?php
require_once '../config/db.php';
require_once '../classes/Chair.php';
require_once '../classes/Subject.php';

$chairs = Chair::all($connection);
$subjects = Subject::all($connection);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Coach</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Add Coach</h1>
            <form action="store.php" method="post" >
                <div class="form-group">
                    <label>Name:<input type="text" name="name" class="form-control"></label>
                    <br>
                    <label>Surname:<input type="text" name="surname" class="form-control"></label>
                    <br>
                    <label>Email:<input type="text" name="email" class="form-control"></label>
                    <br>
                        <label>Chair: <select name="chair" class="form-control ">
                                <?php foreach($chairs as $chair):?>
                                <option value="<?=$chair->getId()?>"><?=$chair->getTitle()?></option>
                                <?php endforeach;?>
                            </select></label>
                    <br>
                        <label>Subject: <select multiple name="subjects[]" class="form-control">
                                <?php foreach($subjects as $subject):?>
                                <option value="<?=$subject->getId()?>"><?=$subject->getTitle()?></option>
                                <?php endforeach;?>
                            </select></label>
                    <br>
                    <button type="submit" class="btn btn-success">submit</button>
                </div>
            </form>
            <a href="../index.php" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
</body>
</html>
