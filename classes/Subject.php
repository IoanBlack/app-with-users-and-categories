<?php


require_once 'DBEntity.php';

class Subject extends DBEntity
{
    protected $id;
    protected $title;


    public function __construct($title)
    {
        parent::__construct();
        $this->title = $title;
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    static public function all(PDO $connection )
    {
        try {
            $statement = $connection->query('SELECT * FROM subjects');
            $subjectsArr = $statement->fetchAll();
            $subjObjs = [];
            foreach ($subjectsArr as $subjArr) {
                $subject = new Subject($subjArr['title']);
                $subject->setId($subjArr['id']);
                $subjObjs[] = $subject;
            }
            return $subjObjs;

        } catch (Exception $exception) {
            echo 'Error getting subjects!' . $exception->getCode() . ' message: ' . $exception->getMessage();
        }
    }

    static public function getById($id, $connection){
        try {
            $statement = $connection->prepare('SELECT * FROM subjects WHERE id=:id');
            $statement->bindValue(':id', $id);
            $statement->execute();
            $subjArr  = $statement->fetchAll() [0];

            $subject = new self($subjArr['title']);
            $subject->setId($subjArr['id']);
            return $subject;
        }catch (Exception $exception){
            echo 'Error getting Subject'.$exception->getCode(). ' message: ' . $exception->getMessage();
        }
    }

   public function store() {
           try {
               $statement = $this->db->prepare('INSERT INTO subjects SET
                    title = :title');
               $statement->bindValue(':title',$this->getTitle());
               $statement->execute();
           } catch (Exception $exception) {
               echo 'Error storing subject!' . $exception->getCode() . ' message: ' . $exception->getMessage();
               die;
           }
   }

    static public function upgrade($id, $title) {
        $subject = new Subject($title);
        $subject->setId($id);
        $subject->update();
    }

    public function update() {
        try {
            $statement = $this->db->prepare('UPDATE subjects SET
                                                        title = :title
                                                        WHERE id = :id');
            $statement->execute([
                ':title' => $this->title,
                ':id' => $this->id,
            ]);
        } catch (Exception $exception){
            echo "Error updating subject! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $connection) {
        $subject = self::getById($id,$connection);
        $subject->deleteSubject($id);
        $subject->destroy($connection);

    }

     public function deleteSubject($subjId) {
        try {
            $coachSubjStatement = $this->db->prepare('DELETE FROM coach_subject WHERE
            subject_id = :subject_id ');
            $coachSubjStatement->bindValue(':subject_id', $subjId);
            $coachSubjStatement->execute();
        } catch (Exception $exception) {
            echo "Error deleting subject! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }


    public function destroy( $connection) {
        try {
            $sql = 'DELETE FROM subjects WHERE id = :id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting subject!' . $exception->getCode() . 'msg:' . $exception->getMessage();
        }
    }

    public function getCoaches($id)
    {
        try {
            $sql = 'SELECT name,surname FROM coaches AS c 
        JOIN coach_subject AS cs ON c.id = cs.coach_id 
        WHERE subject_id = :id';
            $statement = $this->db->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $coaches = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $coaches;
        } catch (Exception $exception) {
            echo 'Error getting coaches!' . $exception->getCode() . ' message: ' . $exception->getMessage();;
            die;
        }
    }


}