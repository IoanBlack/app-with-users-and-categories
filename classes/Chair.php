<?php


require_once 'DBEntity.php';

class Chair extends DBEntity
{
    protected $id = 0;
    protected $title;
    protected $phone;

    public function __construct($title,$phone)
    {
        parent::__construct();
        $this->title = htmlspecialchars($title);
        $this->phone = htmlspecialchars($phone);
    }

    public function store() {
       try {
           $sql = 'INSERT INTO chairs SET
                    title = :title,
                    phone = :phone';
           $statement = $this->db->prepare($sql);
           $statement->bindValue(':title',$this->getTitle());
           $statement->bindValue(':phone',$this->getPhone());
           $statement->execute();
       } catch (Exception $exception) {
           echo 'Error storing chair!' . $exception->getCode() . ' message: ' . $exception->getMessage();
           die;
       }
    }

    static public function all(PDO $connection )
    {
        try {
            $sql = 'SELECT * FROM chairs';
            $statement = $connection->query($sql);
            $chairsArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            $chairObjects = [];
            foreach ($chairsArr as $chairArr) {
                $chair = new Chair($chairArr['title'], $chairArr['phone']);
                $chair->setId($chairArr['id']);
                $chairObjects[] = $chair;
            }
            return $chairObjects;

        } catch (Exception $exception) {
            echo 'Error getting chairs!' . $exception->getCode() . ' message: ' . $exception->getMessage();
        }
    }

    public function getCoaches() {
        try {
            $sql = "SELECT * from coaches WHERE chair_id =".$this->id;
            $statement = $this->db->query($sql);
            $coachesArr = $statement->fetchAll();
            $coachesObjects = [];
            foreach ($coachesArr as $coachArr){
                $coach = new Coach($coachArr['name'], $coachArr['surname'], $coachArr['email'], $coachArr['chair_id']);
                $coach->setId($coachArr['id']);
                $coachesObjects[] = $coach;
            }
            return $coachesObjects;
        } catch (Exception $exception) {
            echo 'Error  getting coaches!' . $exception->getCode() . 'msg: ' . $exception->getMessage();
        }
    }

    static public function getById($id, $connection){
        try {
            $sql = "SELECT * FROM chairs WHERE id=:id";
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $chairsArr = $statement->fetchAll();
            $chairArr = $chairsArr[0];
            $chair = new self($chairArr['title'],$chairArr['phone']);
            $chair->setId($chairArr['id']);
            return $chair;
        }catch (Exception $exception){
            die('Error getting Chair'.$exception->getCode());
        }

    }

    static public function delete($id, PDO $connection) {
        $chair = self::getById($id,$connection);
        $chair->destroy($connection);

    }

     public function destroy( $connection) {
        try {
            $sql = 'DELETE FROM chairs WHERE id = :id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting chair!' . $exception->getCode() . 'msg:' . $exception->getMessage();
        }
    }

    static public function upgrade($id, $title, $phone) {
        $chair = new Chair($title, $phone);
        $chair->setId($id);
        $chair->update();
    }

    public function update(){
        try {
            $sql = 'UPDATE chairs SET
            title = :title,
            phone = :phone
            WHERE id = :id';


            $statement = $this->db->prepare($sql);
            $statement->execute([
                ':title' => $this->title,
                ':phone' => $this->phone,
                ':id' => $this->id,
            ]);

        } catch (Exception $exception){
            echo "Error updating chair! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPhone()
    {
        return $this->phone;
    }


    public function setId($id)
    {
        $this->id = $id;
    }

}