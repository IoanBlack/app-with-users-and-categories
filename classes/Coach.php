<?php



require_once 'DBEntity.php';
require_once 'Chair.php';


class Coach extends DBEntity
{
    protected $id;
    protected $name;
    protected $surname;
    protected $email;
    protected $chairId;


    public function __construct($name,$surname,$email,$chairId = NULL)
    {
        parent::__construct();
        $this->name = htmlspecialchars($name);
        $this->surname = htmlspecialchars($surname);
        $this->email = htmlspecialchars($email);
        if($chairId) {
            $statement = $this->db->prepare('SELECT * FROM chairs WHERE id=:id');
            $statement->bindValue(':id',$chairId);
            $statement->execute();
            $chairArr = $statement->fetchAll();
            $this->chairId = new Chair($chairArr[0]['title'],$chairArr[0]['phone']);
            $this->chairId->setId($chairId);
        }
    }

    public function getChair()
    {
        $statement = $this->db->prepare('SELECT * FROM chairs WHERE id=:id');
        $statement->bindValue(':id', $this->chairId->getId());
        $statement->execute();
        $chairArr = $statement->fetchAll();
        $chair = new Chair($chairArr[0]['title'],$chairArr[0]['phone']);
        $chair->setId($chairArr[0]['id']);
        return $chair;
    }

    static public function getById($id, PDO $connection) {
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECT * FROM coaches WHERE id=:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $coachesArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            $coachArr = $coachesArr[0];
            $coachObj = new self($coachArr['name'], $coachArr['surname'], $coachArr['email'],$coachArr['chair_id']);
            $coachObj->setId($coachArr['id']);
            return $coachObj;
        } catch (Exception $exception){
    echo "Error getting coach! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function all(PDO $connection) {
        try {
            $sql = 'SELECT * FROM coaches';
            $Result = $connection->query($sql);
            $coachesArr = $Result->fetchAll();
            $coachObjects = [];
            foreach ($coachesArr as $coachArr) {
                $coachObj = new self($coachArr['name'], $coachArr['surname'],
                $coachArr['email'],$coachArr['chair_id']);
                $coachObj->setId($coachArr['id']);
                $coachObjects[] = $coachObj;
            }
            return $coachObjects;
        } catch (Exception $exception){
            echo "Error getting coaches! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function store() {
        try {
            $sql = 'INSERT INTO coaches SET 
            name = :name,
            surname = :surname,
            email = :email,
            chair_id = :chair_id';
            $statement = $this->db->prepare($sql);
            $statement->bindValue(':name', $this->name);
            $statement->bindValue(':surname', $this->surname);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':chair_id', $this->chairId->getId());
            $statement->execute();
            $this->id = $this->db->lastInsertId();

        } catch (Exception $exception){
            echo "Error storing coach! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

     public function addSubject($subjIds)
    {
        try {
            $coachSubjStatement = $this->db->prepare('INSERT INTO coach_subject SET 
            coach_id = :coach_id,
            subject_id = :subject_id');

            foreach ($subjIds as $subjId) {
                $coachSubjStatement->bindValue(':coach_id', $this->id);
                $coachSubjStatement->bindValue(':subject_id', $subjId);
                $coachSubjStatement->execute();

            }
        } catch (Exception $exception) {
            echo "Error adding subject! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function deleteSubject($coachId,$connection) {
        try {
            $coachSubjStatement = $connection->prepare('DELETE FROM coach_subject WHERE
            coach_id = :coach_id ');
            $coachSubjStatement->bindValue(':coach_id', $coachId);
            $coachSubjStatement->execute();
        } catch (Exception $exception) {
            echo "Error removing subject! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function upgrade($id, $name, $surname,$email,$chairId,$subjIds) {
        $coach = new Coach($name, $surname,$email,$chairId);
        $coach->setId($id);
        $coach->update();
        $coach->addSubject($subjIds);



    }

    public function update() {
        try {
            $sql = 'UPDATE coaches SET
            name = :name,
            surname = :surname,
            email = :email,
            chair_id = :chair_id
            WHERE id = :id';

            $statement = $this->db->prepare($sql);
            $statement->execute([
                ':name' => $this->name,
                ':surname' => $this->surname,
                ':email' => $this->email,
                ':chair_id' => $this->chairId->getId(),
                ':id' => $this->id,
            ]);
        } catch (Exception $exception){
            echo "Error updating coach! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $connection) {
        try {
            $sql = 'DELETE FROM coaches WHERE id =:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting coach!' . $exception->getCode() . ' message: ' . $exception->getMessage();;
            die;
        }
    }

    public function getSubjects($id)
    {
        try {
            $sql = 'SELECT id,title FROM subjects AS s 
        JOIN coach_subject AS cs ON s.id = cs.subject_id 
        WHERE coach_id = :id';
            $statement = $this->db->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $coachSubjsArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $coachSubjsArr;
        } catch (Exception $exception) {
            echo 'Error getting subjects!' . $exception->getCode() . ' message: ' . $exception->getMessage();;
            die;
        }
    }

//    public function updateSubjects($subjIds){
//        try {
//            $sql = 'UPDATE coach_subject SET
//                    coach_id = :coach_id,
//                    subject_id = :subject_id
//                     WHERE coach_id = :coach_id ';
//            $coachSubjStatement = $this->db->prepare($sql);
//            foreach ($subjIds as $Id) {
//                $coachSubjStatement->bindValue(':coach_id', $this->id);
//                $coachSubjStatement->bindValue(':subject_id', $Id);
//                $coachSubjStatement->execute();
//
//            }
//        } catch (Exception $exception) {
//            echo "Error updating subject! " . $exception->getCode() . ' message: ' . $exception->getMessage();
//            die();
//        }
//    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }


    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }


    public function getChairId()
    {
        return $this->chairId;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

}