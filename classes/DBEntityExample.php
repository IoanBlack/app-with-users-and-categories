<?php

class DBEntity
{
    protected $db;

    public function __construct()
    {
        $host = '';
        $dbUser = '';
        $dbPassword = '';
        $dbName = '';
        try{
            $this->db = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec('SET NAMES "utf8"');
        } catch (Exception $exception) {
            echo "Error connecting to db! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die('Wasn\'t able to connect to db!');
        }
    }
}