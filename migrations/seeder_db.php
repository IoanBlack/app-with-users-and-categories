<?php


require_once '../config/db.php';


$coaches = [
    [
      'name' => 'Bob',
      'surname' => 'Squarepants',
      'email' => 'sponge@mail.com',
      'chair_id' => 1

    ],
    [
        'name' => 'Danny',
        'surname' => 'Fenton',
        'email' => 'ghost@email.com',
        'chair_id' => 1
    ],
    [
        'name' => 'Peppa',
        'surname' => 'Pig',
        'email' => 'oink@mail.com',
        'chair_id' => 3
     ],
    [
        'name' => 'Peter',
        'surname' => 'Parker',
        'email' => 'spider@mail.com',
        'chair_id' => 2
    ],
    [
        'name' => 'Rick',
        'surname' => 'Sanchez',
        'email' => 'alcoscience@mail.com',
        'chair_id' => 3
    ]
];

$subjects = [
    ['title' => 'math'],
    ['title' => 'chemistry'],
    ['title' => 'language'],
    ['title' => 'history']
];

$chairs = [
  [
      'title' => 'historical Chair',
      'phone' => '8-800-555-35-35',
  ],
  [
      'title' => 'Chair of chemistry',
      'phone' => '8-800-555-35-35',
  ],
  [
      'title' => 'Chair of math',
      'phone' => '8-800-555-35-35',
  ]
];



try{
    foreach ($chairs as $chair) {

        $connection->exec( $chairSql = 'INSERT INTO chairs SET 
        title = " ' . $chair['title'] . '",
        phone = " ' . $chair['phone'] . '" ');
    }


    foreach ($coaches as $coach) {

        $connection->exec('INSERT INTO coaches SET 
        name = " ' . $coach['name'] . '",
        surname = " ' . $coach['surname'] . '",
        email = " ' . $coach['email'] . '",
        chair_id = "' . $coach['chair_id'] . '" ');
    }


        foreach ($subjects as $subject) {

        $connection->exec('INSERT INTO subjects SET 
        title = " ' . $subject['title'] . '" ');
        }

    } catch (Exception $exception) {
        echo 'Error inserting test data to tables!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
}

header('Location:../');