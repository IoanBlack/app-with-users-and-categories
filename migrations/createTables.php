<?php

require_once '../config/db.php';

try {
    $chairSql = 'CREATE TABLE chairs(
        id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
        title VARCHAR(255) NOT NULL,
        phone VARCHAR(255) NOT NULL)
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';

    $connection->exec($chairSql);

    $coachSql = 'CREATE TABLE coaches(
        id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
        name VARCHAR(255) NOT NULL,
        surname VARCHAR(255) NOT NULL,
        email VARCHAR (255) NOT NULL,
        chair_id INT NOT NULL,
        FOREIGN KEY (chair_id) REFERENCES chairs (id))
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
        $connection->exec($coachSql);

    $subjSql = 'CREATE TABLE subjects(
        id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
        title VARCHAR(255) NOT NULL)
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
        $connection->exec($subjSql);

    $coach_subjSql = 'CREATE TABLE coach_subject(
        coach_id INT,
        subject_id INT,
        FOREIGN KEY (coach_id) REFERENCES coaches (id) ,
        FOREIGN KEY (subject_id) REFERENCES subjects (id))
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
        $connection->exec($coach_subjSql);



} catch (Exception $exception) {
    echo "Error creating tables! " . $exception->getCode() . ' message: ' . $exception->getMessage();
}

header('/seeder_db');