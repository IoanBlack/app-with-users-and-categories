<?php


require_once '../config/db.php';
require_once '../classes/Chair.php';

if(isset($_GET['id'])){
$chair = Chair::getById($_GET['id'],$connection);
}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Chair</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Edit Chair</h1>
            <form action="update.php" method="post" >
                <div class="form-group">
                    <input type="hidden" name="id" value="<?=$_GET['id']?>">
                    <label>Title:<input type="text" name="title" class="form-control" value="<?=$chair->getTitle()?>"></label>
                    <br>
                    <label>Phone:<input type="text" name="phone" class="form-control" value="<?=$chair->getPhone()?>"></label>
                    <br>
                    <button type="submit" class="btn btn-success">submit</button>
                </div>
            </form>
            <a href="index.php" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
</body>
</html>