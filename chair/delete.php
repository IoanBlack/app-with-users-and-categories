<?php

require_once '../config/db.php';
require_once '../classes/Chair.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    Chair::delete($id, $connection);
  header('Location:index.php');
}