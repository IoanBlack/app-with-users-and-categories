<?php

require_once '../config/db.php';
require_once '../classes/Chair.php';
$chairs = Chair::all($connection)
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chairs</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="../index.php">Coaches</a>
                        <a class="nav-item nav-link" href="index.php">Chairs</a>
                        <a class="nav-item nav-link" href="../subject/index.php">Subjects</a>
                    </div>
                </div>
            </nav>
            <h1>Chairs</h1>
            <a href="create.php" class=" btn btn-success">Add Chair</a>
            <?php if($chairs):?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Phone</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($chairs as $chair):?>
                        <tr>
                            <td><?=$chair->getTitle()?></td>
                            <td><?=$chair->getPhone()?></td>
                            <td>
                                <a href="show.php?id=<?=$chair->getId()?>" class="btn btn-info">Show</a>
                                <a href="edit.php?id=<?=$chair->getId()?>" class="btn btn-warning">Edit</a>
                                <a onclick="return confirm('are you sure?')" href="delete.php?id=<?=$chair->getId()?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>
        </div>
    </div>
</div>
</body>
</html>