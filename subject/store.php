<?php


require_once '../classes/Subject.php';

if (isset($_POST['title'])) {

    $subject = new Subject($_POST['title']);
    $subject->store();
}
header('Location:index.php');
