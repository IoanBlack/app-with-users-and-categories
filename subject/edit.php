<?php


require_once '../config/db.php';
require_once '../classes/Subject.php';

if(isset($_GET['id'])) {
$subject = Subject::getById($_GET['id'],$connection);
}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Subject</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit Subject</h1>
                <form action="update.php" class="form" method="post">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?=$_GET['id']?>">
                        <label>Title: <input type="text" name="title" class="form-control" value="<?=$subject->getTitle()?>"></label>
                        <button class="submit btn btn-success">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>