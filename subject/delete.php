<?php

require_once '../config/db.php';
require_once '../classes/Subject.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    Subject::delete($id, $connection);
    header('Location:index.php');
}