<?php

require_once '../config/db.php';
require_once '../classes/Subject.php';
require_once '../classes/Coach.php';


if(isset($_GET['id'])) {

    $subject = Subject::getById($_GET['id'],$connection);
    $coaches = $subject->getCoaches($_GET['id']);
}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$subject->getTitle()?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?=$subject->getTitle()?></h1>
            <?php if($coaches):?>
                <h3> coaches:</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($coaches as $coach):?>
                        <tr>
                            <td><?=$coach['name']?></td>
                            <td><?=$coach['surname']?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>
            <a href="index.php" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
</body>
</html>


