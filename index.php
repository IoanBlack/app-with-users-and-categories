<?php


require_once 'config/db.php';
require_once 'classes/Coach.php';



$coaches = Coach::all($connection);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
 <div class="container">
     <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="index.php">Coaches</a>
                    <a class="nav-item nav-link" href="chair/index.php">Chairs</a>
                    <a class="nav-item nav-link" href="subject/index.php">Subjects</a>
                </div>
            </div>
            </nav>
         <h1>Coaches</h1>
            <a class="btn btn-success" href="coach/add.php">Add Coach</a>
         <?php if($coaches):?>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Chair</th>
                    <th>Subjects</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($coaches as $coach):?>
                <tr>
                    <td><?=$coach->getName()?></td>
                    <td><?=$coach->getSurname()?></td>
                    <td><?=$coach->getChair()->getTitle()?></td>
                    <td>
                        <?php foreach ($coach->getSubjects($coach->getId()) as $subject) {
                            echo $subject['title'] . '<br>';
                        } ?>
                     </td>

                    <td>
                        <a href="coach/edit.php?id=<?=$coach->getId()?>" class="btn btn-warning">Edit</a>
                        <a onclick="return confirm('are you sure?')" href="coach/delete.php?id=<?=$coach->getId()?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
         <?php endif; ?>
        </div>
     </div>
 </div>
</body>
</html>